# Algebraic Structures



## Purpose

For me to play around with implementing algebraic structures and to learn a bit about Java generics. 

## Permutation Group

The way I generate the HashMap for the permutation group can be found in the following diagram, which was made before coding to keep it simple.

![plot](./src/test/java/generators/Permutations.png)