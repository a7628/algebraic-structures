package generators;

import structures.Group;

import java.util.*;

public class PermutationGroup <V, T extends HashMap<V, V>> extends Group<T> {
    public Set<V> setToPermute;

    public PermutationGroup(Set<V> setToPermute) {
        this.firstOperation = new PermutatationBinaryOperation<>();
        this.setToPermute = setToPermute;
        this.set = generateSet();
        generateIdentity();
        findInverses(firstOperation, identity);
    }

    private void generateIdentity() {
        try {
            @SuppressWarnings("unchecked")
            T identity = (T) new HashMap<V, V>();
            for (V value : setToPermute) {
                identity.put(value, value);
            }

            this.identity = identity;
        } catch (ClassCastException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        Objects.requireNonNull(identity);

        for (V value : setToPermute) {
            identity.put(value, value);
        }
    }

    @SuppressWarnings("unchecked")
    public Set<T> generateSet() {

        if (setToPermute.size() > 10) {
            System.out.println("Generating permutations for more than 10 elements causes OOM!");
            System.exit(1);
        }

        Set<T> ret = new HashSet<>();
        Set<List<V>> set = new HashSet<>();

        for (V element : setToPermute) {
            List<V> tmpList = new ArrayList<>();

            tmpList.add(element);
            set.add(tmpList);
        }

        int setToPermuteSize = setToPermute.size();
        long amountOfPermutations = factorial(setToPermuteSize);

        while (set.size() < amountOfPermutations) {

            Set<List<V>> tmp = new HashSet<>(set);
            set = new HashSet<>();

            for (List<V> element : tmp) {
                List<V> result = new ArrayList<>(setToPermute);
                result.removeAll(element);

                for (V result_element : result) {
                    List<V> tmpList = new ArrayList<>(element);
                    tmpList.add(result_element);
                    set.add(tmpList);
                }
            }
        }

        for (List<V> list : set) {
            int i = 0;
            Map<V, V> retElement = new HashMap<>();

            for (V element : setToPermute) {
                if (!list.contains(element)) {
                    list.add(element);
                }
                retElement.put(element, list.get(i++));
            }

            try {
                ret.add((T) retElement);
            } catch (ClassCastException e) {
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }

        return ret;
    }

    public long factorial(long n) {
        long ret = 1;

        for (long i=2; i<=n; i++) {
            ret *= i;
        }

        return ret;
    }
}
