package generators;

import util.BinaryFunction;

import java.util.HashMap;
import java.util.Objects;

public class PermutatationBinaryOperation <V, T extends HashMap<V, V>> implements BinaryFunction<T> {

    private T ret;

    @Override
    public T apply(T t1, T t2) {
        try {
            @SuppressWarnings("unchecked")
            T ret = (T) new HashMap<V, V>();
            this.ret = ret;
        } catch (ClassCastException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        Objects.requireNonNull(ret);

        for (V value : t1.values()) {
            ret.put(value, t1.get(t2.get(value)));
        }

        return ret;
    }
}
