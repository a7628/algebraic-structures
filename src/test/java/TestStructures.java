import generators.PermutationGroup;
import org.junit.Test;
import structures.Magma;
import structures.SemiGroup;
import util.BinaryFunction;
import util.Sets;
import util.SubStructure;

import java.util.*;

public class TestStructures {

    @Test
    public void testGenerationOfPermutationSet() {
        Set<Integer> integerSet = Sets.generateIntegerSet(0, 5);

        PermutationGroup<Integer, HashMap<Integer, Integer>> group = new PermutationGroup<>(integerSet);
        Set<HashMap<Integer, Integer>> permutationSet = group.generateSet();

        System.out.println(permutationSet.size());
        System.out.println(group.verify());
        System.out.println(group.checkAbelian(group.firstOperation));

    }

    @Test
    public void checkSubset() {
        Set<Integer> set = Sets.generateIntegerSet(0, 10);
        Set<Integer> subset = Sets.generateIntegerSet(2, 8, 2);

        System.out.println(Sets.checkIfSubset(subset, set));
        System.out.println(Sets.checkIfSubset(set, subset));
    }

    @Test
    public void checkSubstructure() {
        BinaryFunction<Integer> operation = (n, m) -> (n + m) % 10;
        Set<Integer> set = Sets.generateIntegerSet(0, 10);
        Set<Integer> subSet1 = Sets.generateIntegerSet(0, 10, 2);
        Set<Integer> subSet2 = Sets.generateIntegerSet(0, 5);

        Magma<Integer> magma = new Magma<>(set, operation);
        Magma<Integer> subMagma1 = new Magma<>(subSet1, operation);
        Magma<Integer> subMagma2 = new Magma<>(subSet2, operation);

        SemiGroup<Integer> semiGroup = new SemiGroup<>(set, operation);
        SemiGroup<Integer> subSemiGroup1 = new SemiGroup<>(subSet1, operation);
        SemiGroup<Integer> subSemiGroup2 = new SemiGroup<>(subSet2, operation);

        System.out.println(SubStructure.checkIfSubStructure(subMagma1, magma));
        System.out.println(SubStructure.checkIfSubStructure(subMagma2, magma));
        System.out.println(SubStructure.checkIfSubStructure(subSemiGroup1, semiGroup));
        System.out.println(SubStructure.checkIfSubStructure(subSemiGroup2, semiGroup));
    }

//    public static void main(String[] args) {
//        Set<Integer> set1 = Stream.of(0, 1, 2, 3, 4).collect(Collectors.toSet());
//        BinaryFunction<Integer> operation1 = (n, m) -> (n + m) % 5;
//
//        Set<Integer> set2 = Stream.of(1, 2, 3, 4).collect(Collectors.toSet());
//        BinaryFunction<Integer> operation2 = (n, m) -> (n + m) % 5;
//
//        Set<Integer> set3 = Stream.of(1, 2, 3, 4).collect(Collectors.toSet());
//        BinaryFunction<Integer> operation3 = (n, m) -> (n * m) % 5;
//
//        Set<Set<Integer>> set4 = new HashSet<>();
//        for (int i=0; i<5; i++) {
//            Set<Integer> tmp = new HashSet<>();
//            for (int j=0; j<=i; j++) {
//                tmp.add(j);
//            }
//            set4.add(tmp);
//        }
//        BinaryFunction<Set<Integer>> operation4 = (s1, s2) -> {
//            Set<Integer> ret = new HashSet<>(s1);
//            ret.retainAll(s2);
//            return ret;
//        };
//
//        Magma<Integer> magma1 = new Magma<>(set1, operation1);
//        Magma<Integer> magma2 = new Magma<>(set2, operation2);
//        Magma<Integer> magma3 = new Magma<>(set3, operation3);
//        Magma<Set<Integer>> magma4 = new Magma<>(set4, operation4);
//
//        SemiGroup<Set<Integer>> semiGroup1 = new SemiGroup<>(set4, operation4);
//
//        Monoid<Integer> monoid1 = new Monoid<>(set1, operation1, 0);
//        Monoid<Integer> monoid2 = new Monoid<>(set1, operation1, 5);
//        Monoid<Set<Integer>> monoid3 = new Monoid<>(set4, operation4, new HashSet<>());
//        Monoid<Set<Integer>> monoid4 = new Monoid<>(set4, operation4, Stream.of(0, 1).collect(Collectors.toSet()));
//        Monoid<Set<Integer>> monoid5 = new Monoid<>(set4, operation4, Stream.of(0, 1, 2, 3, 4).collect(Collectors.toSet()));
//
//        Group<Integer> group1 = new Group<>(set1, operation1, 0);
//        Group<Set<Integer>> group2 = new Group<>(set4, operation4, Stream.of(0, 1, 2, 3, 4).collect(Collectors.toSet()));
//
//        Ring<Integer> ring = new Ring<>(set1, operation1, 0, operation3, 1);
//
//        System.out.println("magma1: " + magma1.verify()); // true
//        System.out.println("magma2: " + magma2.verify()); // false
//        System.out.println("magma3: " + magma3.verify()); // true
//        System.out.println("magma4: " + magma4.verify()); // true
//        System.out.println("---");
//        System.out.println("semiGroup1: " + semiGroup1.verify()); // true
//        System.out.println("---");
//        System.out.println("monoid1: " + monoid1.verify()); // true
//        System.out.println("monoid2: " + monoid2.verify()); // false
//        System.out.println("monoid3: " + monoid3.verify()); // false
//        System.out.println("monoid4: " + monoid4.verify()); // false
//        System.out.println("monoid5: " + monoid5.verify()); // true
//        System.out.println("---");
//        System.out.println("group1: " + group1.verify()); // true
//        System.out.println("group2: " + group2.verify()); // false
//        System.out.println("---");
//        System.out.println("ring: " + ring.verify()); // true
//    }
}
