package util;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sets {

    public static Set<Integer> generateIntegerSet(int lowerBound, int upperBound) {
        return generateIntegerSet(lowerBound, upperBound, 1);
    }

    public static Set<Integer> generateIntegerSet(int lowerBound, int upperBound, int step) {
        int limit = Math.floorDiv(upperBound-lowerBound+step, step);

        return Stream.iterate(lowerBound, n -> n+step).limit(limit).collect(Collectors.toSet());
    }

    public static <T> boolean checkIfSubset(Set<T> subset, Set<T> set) {
        Set<T> copySet = new HashSet<>(set);
        copySet.retainAll(subset);

        return copySet.equals(subset);
    }
}
