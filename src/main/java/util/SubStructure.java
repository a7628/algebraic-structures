package util;

import structures.Group;
import structures.Magma;
import structures.Monoid;
import structures.Ring;

public class SubStructure {

    public static <V, T extends Magma<V>> boolean checkIfSubStructure(T subStructure, T structure) {
        String structureClass = subStructure.getClass().getName();

        return switch (structureClass) {
            case "structures.Magma" -> checkMagma(subStructure, structure);
            case "structures.SemiGroup" -> checkSemiGroup(subStructure, structure);
            case "structures.Monoid" -> checkMonoid(subStructure, structure);
            case "structures.Group" -> checkGroup(subStructure, structure);
            case "structures.Ring" -> checkRing(subStructure, structure);
            default -> false;
        };
    }

    private static <V, T extends Magma<V>> boolean checkMagma(T subMagma, T magma) {
        if (!Sets.checkIfSubset(subMagma.set, magma.set)) {
            return false;
        }

        for (V element1 : subMagma.set) {
            for (V element2 : subMagma.set) {
                if (!subMagma.set.contains(magma.firstOperation.apply(element1, element2))) {
                    return false;
                }
            }
        }

        return true;
    }

    private static <V, T extends Magma<V>> boolean checkSemiGroup(T subSemiGroup, T semiGroup) {
        // has same requirements as Magma for sub-structure
        return checkMagma(subSemiGroup, semiGroup);
    }

    private static <V, T extends Magma<V>> boolean checkMonoid(T subMonoid, T monoid) {
        @SuppressWarnings("unchecked")
        Monoid<V> castMonoid = (Monoid<V>) monoid;
        boolean hasIdentity = subMonoid.set.contains(castMonoid.identity);

        return hasIdentity && checkSemiGroup(subMonoid, monoid);
    }

    private static <V, T extends Magma<V>> boolean checkGroup(T subGroup, T group) {
        // has same requirements as Monoid for sub-structure (when finite)
        return checkMonoid(subGroup, group);
    }

    private static <V, T extends Magma<V>> boolean checkRing(T subRing, T ring) {
        @SuppressWarnings("unchecked")
        Ring<V> castSubRing = (Ring<V>) subRing;
        @SuppressWarnings("unchecked")
        Ring<V> castRing = (Ring<V>) ring;

        Monoid<V> subMonoid = new Monoid<>(subRing.set, castSubRing.firstOperation, castSubRing.identity);
        Monoid<V> monoid = new Monoid<>(ring.set, castRing.firstOperation, castRing.identity);

        Group<V> subGroup = new Group<>(subRing.set, castSubRing.secondOperation, castSubRing.secondIdentity);
        Group<V> group = new Group<>(ring.set, castRing.secondOperation, castRing.secondIdentity);

        return checkMonoid(subMonoid, monoid) && checkGroup(subGroup, group);
    }

}
