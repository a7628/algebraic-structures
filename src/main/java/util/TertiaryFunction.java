package util;

@FunctionalInterface
public interface TertiaryFunction<T> {

    T apply(T t1, T t2, T t3);
}
