package util;

public class Errors {
    public static void nullOperatorCheck(BinaryFunction<?> operator, String c) {
        if (operator == null) {
            System.out.println("No binary operation defined in " + c);
            System.exit(1);
        }
    }
}
