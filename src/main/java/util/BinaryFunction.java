package util;

import java.util.Objects;
import static java.lang.Math.abs;

@FunctionalInterface
public interface BinaryFunction<T> {

    T apply(T t1, T t2);

    default BinaryFunction<T> compose(BinaryFunction<T> before, Integer n) {
        Objects.requireNonNull(before);
        Objects.requireNonNull(n);
        // Only 0 or 1 as input would work, so converting input to 0 or 1. 0 = false, 1 = true
        boolean whichVariable = abs(n) % 2 == 1;

        return whichVariable
                ? (T t1, T t2) -> apply(before.apply(t1, t2), t2)
                : (T t1, T t2) -> apply(t1, before.apply(t1, t2));
    }

    default BinaryFunction<T> andThen(BinaryFunction<T> after, Integer n) {
        Objects.requireNonNull(after);
        Objects.requireNonNull(n);
        // Only 0 or 1 as input would work, so converting input to 0 or 1. 0 = false, 1 = true
        boolean whichVariable = abs(n) % 2 == 1;

        return whichVariable
                ? (T t1, T t2) -> after.apply(apply(t1, t2), t2)
                : (T t1, T t2) -> after.apply(t1, apply(t1, t2));
    }

    default TertiaryFunction<T> leftDistribute(BinaryFunction<T> second) {
        Objects.requireNonNull(second);

        return (T t1, T t2, T t3) -> apply(second.apply(t1, t2), second.apply(t1, t3));
    }

    default TertiaryFunction<T> rightDistribute(BinaryFunction<T> second) {
        Objects.requireNonNull(second);

        return (T t1, T t2, T t3) -> apply(second.apply(t2, t1), second.apply(t3, t1));
    }
}
