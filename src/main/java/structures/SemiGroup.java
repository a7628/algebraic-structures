package structures;

import util.BinaryFunction;
import util.Errors;

import java.util.Set;

public class SemiGroup<T> extends Magma<T> {

    public SemiGroup() {
    }

    public SemiGroup(Set<T> set, BinaryFunction<T> firstOperation) {
        super(set, firstOperation);
    }

    public boolean checkAssociative(BinaryFunction<T> operation) {
        Errors.nullOperatorCheck(operation, this.getClass().getName());

        for (T element1 : this.set) {
            for (T element2 : this.set) {
                for (T element3 : this.set) {
                    T part1result1 = operation.apply(element1, element2);
                    T result1 = operation.apply(part1result1, element3);
                    T part1result2 = operation.apply(element2, element3);
                    T result2 = operation.apply(element1, part1result2);
                    if (!result1.equals(result2)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Override
    public boolean verify() {
        return super.verify() && checkAssociative(firstOperation);
    }
}
