package structures;

import util.BinaryFunction;

import java.util.Set;

public class Monoid <T> extends SemiGroup<T> {
    public T identity;

    public Monoid() {
    }

    public Monoid(Set<T> set, BinaryFunction<T> firstOperation, T identity) {
        super(set, firstOperation);
        this.identity = identity;
    }

    public boolean checkIdentity(BinaryFunction<T> operation, T identity) {
        if (!set.contains(identity)) {
            System.out.println("Identity " + identity + " doesn't belong to set " + set);
            return false;
        }

        for (T element : set) {
            if (!operation.apply(element, identity).equals(element)) {
                return false;
            } else if (!operation.apply(identity, element).equals(element)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean verify() {
        return super.verify() && checkIdentity(firstOperation, identity);
    }
}
