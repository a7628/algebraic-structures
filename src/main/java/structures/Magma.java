package structures;

import util.BinaryFunction;
import util.Errors;


import java.util.HashSet;
import java.util.Set;

public class Magma <T> {
    public Set<T> set = new HashSet<>();
    public BinaryFunction<T> firstOperation;

    public Magma() {
    }

    public Magma(Set<T> set, BinaryFunction<T> firstOperation) {
        this.set = set;
        this.firstOperation = firstOperation;
    }

    public boolean checkClosed(BinaryFunction<T> operation) {
        Errors.nullOperatorCheck(operation, this.getClass().getName());

        for (T element1 : set) {
            for (T element2 : set) {
                T result = operation.apply(element1, element2);

                if (!set.contains(result)) {
                    return false;
                }
            }
        }

        return true;
    }

    public boolean verify() {
        return checkClosed(firstOperation);
    }
}
