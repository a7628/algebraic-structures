package structures;

import util.Errors;
import util.BinaryFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Group <T> extends Monoid<T> {
    public Map<T, T> inverses = new HashMap<>();

    public Group() {
    }

    public Group(Set<T> set, BinaryFunction<T> firstOperation, T identity) {
        super(set, firstOperation, identity);
        findInverses(firstOperation, identity);
    }

    public Group(Set<T> set, BinaryFunction<T> firstOperation, T identity, Map<T, T> inverses) {
        super(set, firstOperation, identity);
        this.inverses = inverses;
    }

    public void findInverses(BinaryFunction<T> operation, T identity) {
        Errors.nullOperatorCheck(operation, this.getClass().getName());

        if (!checkIdentity(operation, identity)) {
            System.out.println("Identity " + identity + " is not a valid identity!");
        }

        for (T element : set) {
            for (T potentialInverse : set) {
                if (operation.apply(element, potentialInverse).equals(identity)) {
                    inverses.put(element, potentialInverse);
                    break;
                }
            }
        }
    }

    public boolean checkInverses(BinaryFunction<T> operation, T identity) {
        if (!inverses.keySet().equals(set)) {
            System.out.println("Inverses " + inverses + " doesn't contain every element of set " + set.toString());
            return false;
        }

        for (T element : set) {
            T inverse = inverses.get(element);
            if (!operation.apply(element, inverse).equals(identity)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkAbelian(BinaryFunction<T> operation) {
        if (!this.checkClosed(operation)) {
            System.out.println("structures.Group not closed!");
            return false;
        }
        if (!this.checkIdentity(operation, identity)) {
            System.out.println("structures.Group no identity!");
            return false;
        }
        if (!this.checkAssociative(operation)) {
            System.out.println("structures.Group not associative!");
            return false;
        }
        if (!this.checkInverses(operation, identity)) {
            System.out.println("structures.Group no inverses!");
            return false;
        }

        for (T element1 : set) {
            for (T element2 : set) {
                T firstResult = operation.apply(element1, element2);
                T secondResult = operation.apply(element2, element1);
                if (!firstResult.equals(secondResult)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean verify() {
        return super.verify() && checkInverses(firstOperation, identity);
    }
}
