package structures;

import util.BinaryFunction;
import util.TertiaryFunction;

import java.util.Map;
import java.util.Set;

public class Ring <T> extends Group<T> {
    public BinaryFunction<T> secondOperation;
    public T secondIdentity;

    public Ring() {
    }

    public Ring(Set<T> set, BinaryFunction<T> firstOperation, T identity, BinaryFunction<T> secondOperation, T secondIdentity) {
        super(set, firstOperation, identity);
        findInverses(firstOperation, identity);
        this.secondOperation = secondOperation;
        this.secondIdentity = secondIdentity;
    }

    public Ring(Set<T> set, BinaryFunction<T> firstOperation, T identity, Map<T, T> inverses, BinaryFunction<T> secondOperation, T secondIdentity) {
        super(set, firstOperation, identity, inverses);
        this.secondOperation = secondOperation;
        this.secondIdentity = secondIdentity;
    }

    public boolean checkDistributive() {
        TertiaryFunction<T> leftDistribution = firstOperation.leftDistribute(secondOperation);
        TertiaryFunction<T> rightDistribution = firstOperation.rightDistribute(secondOperation);
        for (T a : set) {
            for (T b : set) {
                for (T c : set) {
                    boolean left = secondOperation.apply(a, firstOperation.apply(b, c))
                            == leftDistribution.apply(a, b, c);
                    boolean right = secondOperation.apply(firstOperation.apply(b, c), a)
                            == rightDistribution.apply(a, b, c);

                    if (!(left && right)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Override
    public boolean verify() {
        return checkAbelian(firstOperation)
                && checkDistributive()
                && checkAssociative(secondOperation)
                && checkClosed(secondOperation)
                && checkIdentity(secondOperation, secondIdentity);
    }
}
